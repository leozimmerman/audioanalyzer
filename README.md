# AudioAnalyzer

Introduction
-------
This app uses the ofxAudioAnalyzer addon to analyze a two channel audio sygnal in real-time. It wraps the library Essentia](http://essentia.upf.edu/) providing the following algorithms. 

- RMS, Instant power, Energy.
- Pitch frequency, Pitch Confidence, Pitch Salience.
- Tuning Frequency.
- Onsets.
- FFT, Mel Bands, MFCC
- Harmonic Pitch Class Profile
- HFC, Centroid, Inharmonicity, Spectral Complexity. 
Algorithm reference: http://essentia.upf.edu/documentation/algorithms_reference.html

Demo video: https://vimeo.com/129795472


Compatibility
---------
Only OSX and Linux

- OSX (!): needs the OF 64 bits version of Nick Hardeman : https://github.com/NickHardeman/openframeworks_osx_64

- Linux 64 bits. Tested with Ubuntu 12.04


Dependencies
---------
- ofxUI (github.com/rezaali/ofxUI)
- ofxPd (github.com/danomatika/ofxPd)
- ofxAudioAnalyzer (github.com/leozimmerman/ofxAudioAnalyzer)



License
------------
Essentia library and this addon are distributed under  Affero GPLv3 license.