#include "testApp.h"


//--------------------------------------------------------------
void testApp::setup(){
    
    ofBackground(CLARO);
	ofSetFrameRate(60);
    ofSetVerticalSync(true);
    
    bNarrowView = bShowHelp = false;
    int inChan, outChan;
    outChan = O_CH;
    inChan = I_CH;

    ///--------------------------------------------------------

    maxEnergy = 230.0;
    minPitch = 130.0;//-C3
    maxPitch = 2093.0;//-C7
    maxOnHfc = 536000;
    maxOnCmpx = 15.0;
    maxOnFlux = 15.0;
    maxHfc = 1000.0;
    maxCentroid = SR/2;
    maxSpectralComp = 40.0;
    maxDct = 300.0;

    ///-------------------------------

    int sampleRate = SR;
    int bufferSize = BS;
    int ticksPerBuffer = bufferSize/64;


    buffer_1 =  new float[bufferSize];
    buffer_2 =  new float[bufferSize];
    for(int i = 0; i < bufferSize; i++) { buffer_1[i]=buffer_2[i]= 0;}

    audioAnalyzer1.setup(bufferSize, sampleRate);
    audioAnalyzer2.setup(bufferSize, sampleRate);
    
    //para seleccionar otro dispositivo de entrada de Audio:
    //soundStream.setDeviceID(0);

    soundStream.setup(this, outChan, inChan, sampleRate, bufferSize, ticksPerBuffer);
    core.setup(outChan, inChan, sampleRate, ticksPerBuffer);

    filename = "-none-";
    settings_name = "-none-";

    guiWidth = ofGetWidth()/6;
    guiHeight = ofGetHeight();

    setGUI0();
    setGUI1();
    setGUI2();
    setGUI3();
    setGUI4();
    setGUI5();
    setGUI6();

	m1vol=m2vol=1.0;

    iMode = FILES;
    mic1Volume->setVisible(false);
    mic2Volume->setVisible(false);
    mic1Mute->setVisible(false);
    mic2Mute->setVisible(false);


}
//--------------------------------------------------------------
void testApp::update(){
    
    if (gui1!=NULL) ch1update();
    if (gui4!=NULL) ch2update();

}
//--------------------------------------------------------------
void testApp::draw(){
    //Monitor------
    if(!gui0->isEnabled()){
        ofPushStyle();
   
        string audioInfo = "**** AUDIO INFO ****"
                            "\n\nCH-1-Power: " + ofToString(ch1.powerSlider->getValue())
                            +"\nCH-2-Power: " + ofToString(ch2.powerSlider->getValue())
                            +"\n----------"
                            + "\nSettings Loaded:\n" + settings_name
                            +"\n----------"
                            + "\nInput Mode: " + ofToString(iMode)
                            + "\nKey Pressed: " + ofToString(ofGetKeyPressed())
                            +"\n\n\n*** MONITOR INFO ***\n\n" + monitorInfo;
        
        ofDrawBitmapString(audioInfo, 10,20);
        ofPopStyle();
    }
    ofPushStyle();
        if (gui0->isEnabled()){
            ofSetColor(GRIS);
            ofRect(0,0,guiWidth,ofGetHeight());
        }
        if(gui1->isEnabled()){
            ofSetColor(OSCURO);
            ofRect(guiWidth, 0, guiWidth*0.5, ofGetHeight());
        }
        if(gui2->isEnabled()){
            ofSetColor(OSCURO);
            ofRect(guiWidth*1.5, 0, guiWidth, ofGetHeight());
        }
        if(gui3->isEnabled()){
            ofSetColor(OSCURO);
            ofRect(guiWidth*2.5, 0, guiWidth, ofGetHeight());
        }
        if(gui4->isEnabled()){
            ofSetColor(CLARO);
            ofRect(guiWidth*3.5, 0, guiWidth*0.5, ofGetHeight());
        }
        if(gui5->isEnabled()){
            ofSetColor(CLARO);
            ofRect(guiWidth*4, 0, guiWidth, ofGetHeight());
        }
        if(gui6->isEnabled()){
            ofSetColor(CLARO);
            ofRect(guiWidth*5, 0, guiWidth, ofGetHeight());
        }

    ofPopStyle();

    if (bShowHelp) {
        string help = "'i' info / 'h' help / 'n' narrow view /'m' monitor view / 'r' reset Onsets ";
        ofDrawBitmapStringHighlight(help, 10, ofGetHeight()-15);
    }
   
}
//--------------------------------------------------------------
void testApp::exit(){
  cout<<"Closing..."<<endl;
   // clean up:
    soundStream.stop();
    soundStream.close();
    cout<<"SoundStream Stopped"<<endl;
  audioAnalyzer1.exit();
  audioAnalyzer2.exit();
    cout<<"essenCores deleted"<<endl;
  core.exit();
    cout<<"pdCore deleted"<<endl;


  deletePointers();
    cout<<"pointers Deleted"<<endl;

  cout<<"*******************************"<<endl;
  cout<<"CLOSED SUCCESFULLY!--"<<endl;
  cout<<"*******************************"<<endl;


}
//--------------------------------------------------------------
void testApp::setGUI0(){

    float dim = 16;

 
    //INPUT-SETTINGS************************************
    gui0 = new ofxUISuperCanvas("INPUT SETTINGS", 0, 0, guiWidth, guiHeight);
    gui0->setGlobalSpacerHeight(5.0);

    gui0->addSpacer();
    //INPUT-MODE--------------------------------
	gui0->setWidgetFontSize(OFX_UI_FONT_LARGE);
	vector<string> inModes;
    inModes.push_back("MICS");
	inModes.push_back("AUDIO FILE");
	gui0->addWidgetDown(new ofxUILabel("Input Mode", OFX_UI_FONT_SMALL));
    gui0->addRadio("INPUT-MODE", inModes, OFX_UI_ORIENTATION_HORIZONTAL);
    gui0->addSpacer();
    //OPEN-FILE---------------------------
	gui0->addWidgetDown(new ofxUILabel("OPEN STEREO FILE", OFX_UI_FONT_SMALL));
	gui0->setWidgetFontSize(OFX_UI_FONT_MEDIUM);
	textInput = gui0->addTextInput("OPEN FILE", "*.wav");
    textInput->setAutoUnfocus(false);
    string textString = "File loaded: "+ filename;
    fileDisplay = gui0->addTextArea("textarea", textString, OFX_UI_FONT_SMALL);
	filePlay = gui0->addButton("PLAY", false, dim, dim);
	fileStop = gui0->addButton("STOP", false, dim, dim);
	fileVolume1 = gui0->addMinimalSlider("ch1 Vol", 0, 1.00, 1.0);
	fileVolume2 = gui0->addMinimalSlider("ch2 Vol", 0, 1.00, 1.0);
	fileVolume1->getLabelWidget()->setColorFill(ofColor(0));
	fileVolume2->getLabelWidget()->setColorFill(ofColor(0));

    gui0->addSpacer();
    //MICROPHONES-----------------
    gui0->setWidgetFontSize(OFX_UI_FONT_SMALL);
    gui0->addWidgetDown(new ofxUILabel("MICROPHONES", OFX_UI_FONT_SMALL));
    mic1Volume = gui0->addMinimalSlider("MIC-1 Vol", 0, 1.00, 1.0);
    mic1Volume->getLabelWidget()->setColorFill(ofColor(0));
    mic1Mute = gui0->addToggle( "Mute-1", false);
    mic2Volume = gui0->addMinimalSlider("MIC-2 Vol", 0, 1.00, 1.0);
    mic2Volume->getLabelWidget()->setColorFill(ofColor(0));
    mic2Mute = gui0->addToggle( "Mute-2", false);
    gui0->addSpacer();
    //WAVES DISPLAYS----------------
    gui0->addLabel("Channel 1");
	gui0->addWaveform("WAVEFORM", buffer_1, BS);
	//gui0->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));///-space
	gui0->addLabel("Channel 2");
	gui0->addWaveform("WAVEFORM", buffer_2, BS);
    gui0->addSpacer();///-space
    //INFO-----------
    gui0->setWidgetFontSize(OFX_UI_FONT_MEDIUM);
	gui0->addToggle( "AUDIO-ON/OFF", true, dim, dim);
	gui0->addToggle( "SEND OSC", true, dim, dim);

    gui0->addSpacer();///-space
    vector<string> names;
    names.push_back("settings_1");
    names.push_back("settings_2");
    
    gui0->addButton("SAVE", false);
    ddl = gui0->addDropDownList("SETTINGS FILE", names);
    ddl->setShowCurrentSelected(true);
    gui0->addButton("LOAD", false);
    
    gui0->addSpacer();///-space
    narrowView = gui0->addToggle( "NARROW VIEW", false, dim, dim);
	
    ///******
    gui0->autoSizeToFitWidgets();
	ofAddListener(gui0->newGUIEvent,this,&testApp::guiEvent_0);
	gui0->setDrawBack(false);

}
//--------------------------------------------------------------
void testApp::setGUI1(){
    gui1 = new ofxUISuperCanvas("Chan-1", guiWidth*1, 0, guiWidth*0.5, guiHeight);
    gui1->setGlobalSpacerHeight(5.0);

    gui1->addSpacer();///---------------------------------
    gui1->addToggle("RMS", true);
    gui1->addToggle("Energy", true);
    gui1->addToggle("Power", true);
    ch1.intenSmooth = gui1->addMinimalSlider("SmthInt", 0.0, 1.0, 0.0);
    gui1->setGlobalSpacerHeight(2.0);
    gui1->addSpacer();///---------------------------------
    gui1->addToggle("Pitch", true);
    gui1->addToggle("Tuning", true);
    ch1.pitchSmooth = gui1->addMinimalSlider("SmthPtch", 0.0, 1.0, 0.0);
    gui1->addSpacer();///---------------------------------
    gui1->addToggle("Onsets", true);
    gui1->addMinimalSlider("Alpha", 0.0, 1.0, 0.1);
    gui1->addMinimalSlider("Treshold", 0.0, 1.0, 0.02);
    gui1->addToggle("Hfc", true);
    gui1->addToggle("Complex", true);
    gui1->addToggle("Flux", true);
    gui1->addSpacer();///---------------------------------
    gui1->addToggle("MelBands", true);
    gui1->addToggle("MFCC", true);
    gui1->addToggle("HPCP", true);
    gui1->addSpacer();///---------------------------------
    gui1->addToggle("HFC", true);
    gui1->addToggle("Centroid", true);
    gui1->addToggle("SpcCmplx", true);
    gui1->addToggle("Inharmon", true);
    ch1.specSmooth = gui1->addMinimalSlider("SmthSpc", 0.0, 1.0, 0.0);
    gui1->addSpacer();///---------------------------------
    gui1->addLabel("RESET");
    gui1->addLabel("   ONSETS");
    gui1->addButton("resetOnsets", false, guiWidth*0.45, 24);
    gui1->addSpacer();///---------------------------------
    

   // gui1->autoSizeToFitWidgets();
   	gui1->setDrawBack(true);
    ofAddListener(gui1->newGUIEvent,this,&testApp::guiEvent_1);

}
//--------------------------------------------------------------
void testApp::setGUI4(){

    gui4 = new ofxUISuperCanvas("Chan-2", guiWidth*3.5, 0, guiWidth*0.5, guiHeight);
    gui4->setGlobalSpacerHeight(5.0);

    gui4->addSpacer();///---------------------------------
    gui4->addToggle("RMS", true);
    gui4->addToggle("Energy", true);
    gui4->addToggle("Power", true);
    ch2.intenSmooth = gui4->addMinimalSlider("SmthInt", 0.0, 1.0, 0.0);
    gui4->setGlobalSpacerHeight(2.0);
    gui4->addSpacer();///---------------------------------
    gui4->addToggle("Pitch", true);
    gui4->addToggle("Tuning", true);
    ch2.pitchSmooth = gui4->addMinimalSlider("SmthPtch", 0.0, 1.0, 0.0);
    gui4->addSpacer();///---------------------------------
    gui4->addToggle("Onsets", true);
    gui4->addMinimalSlider("Alpha", 0.0, 1.0, 0.1);
    gui4->addMinimalSlider("Treshold", 0.0, 1.0, 0.02);
    gui4->addToggle("Hfc", true);
    gui4->addToggle("Complex", true);
    gui4->addToggle("Flux", true);
    gui4->addSpacer();///---------------------------------
    gui4->addToggle("MelBands", true);
    gui4->addToggle("MFCC", true);
    gui4->addToggle("HPCP", true);
    gui4->addSpacer();///---------------------------------
    gui4->addToggle("HFC", true);
    gui4->addToggle("Centroid", true);
    gui4->addToggle("SpcCmplx", true);
    gui4->addToggle("Inharmon", true);
    ch2.specSmooth = gui4->addMinimalSlider("SmthSpc", 0.0, 1.0, 0.0);
    gui4->addSpacer();///---------------------------------
    gui4->addLabel("RESET");
    gui4->addLabel("   ONSETS");
    gui4->addButton("resetOnsets", false, guiWidth*0.45, 24);
    gui4->addSpacer();///---------------------------------


   // gui1->autoSizeToFitWidgets();
   	gui4->setDrawBack(true);
    ofAddListener(gui4->newGUIEvent,this,&testApp::guiEvent_4);

}
//--------------------------------------------------------------
void testApp::setGUI2(){

    gui2 = new ofxUISuperCanvas("Channel 1", guiWidth*1.5, 0, guiWidth, guiHeight);
    gui2->setGlobalSliderHeight(10.0);
    gui2-> setWidgetFontSize(OFX_UI_FONT_SMALL);
    gui2->setGlobalGraphHeight(50.0);
    gui2->setGlobalSpacerHeight(5.0);

    gui2->addSpacer();///---------------------------------
    gui2->addLabel("INTENSITY");
	ch1.rmsSlider    = gui2->addSlider("RMS", log10(0.001), log10(1.0), log10(0.001));
	ch1.energySlider = gui2->addSlider("ENERGY", 0.0, maxEnergy, 0.0);
	ch1.powerSlider  = gui2->addSlider("POWER", log10(0.000001), log10(1.0), log10(0.001));

    vector<float> energyArray;
    for(int i = 0; i < BS; i++) energyArray.push_back(log10(0.000001));
    ch1.mgPower = gui2->addMovingGraph("EnergyGraph", energyArray, BS, log10(0.000001), log10(1.0));

    gui2->addSpacer();///------------------------------------
    gui2->addLabel("PITCH");
    ch1.mgPitch = gui2->addMovingGraph("PitchGraph", energyArray, BS, log2(minPitch), log2(maxPitch));
    ch1.pitchSlider = gui2->addSlider("PITCH Frequency", minPitch, maxPitch, 0.0);
    ch1.mgConf = gui2->addMovingGraph("ConfGraph", energyArray, BS, 0.0, 1.0);
    ch1.pitchConfSlider = gui2->addSlider("PITCH Confidence", 0.0, 1.0, 0.0);
    ch1.pitchSalienceSlider = gui2->addSlider("PITCH Salience", 0.0, 1.0, 0.0);
    ch1.tuningDialer = gui2->addNumberDialer("Tuning Freq.", 400.0, 500.0, 440.0, 1);
    ch1.centsDialer =  gui2->addNumberDialer("Cents Dev.", -35.0, 65.0, 0.0, 1);
    gui2->addSpacer();///------------------------------------
    gui2->addLabel("ONSETS");
    ch1.onsetsToggle = gui2->addToggle( "Onsets", false, 60.0, 30.0);
    ch1.onsetHfcSlider = gui2->addSlider("Onset HFC", log10(1.0), log10(maxOnHfc), 0.0);
    ch1.onsetComplexSlider = gui2->addSlider("Onset COMPLEX", 0.0, maxOnCmpx, 0.0);
    ch1.onsetFluxSlider = gui2->addSlider("Onset FLUX", 0.0, maxOnFlux, 0.0);

    gui2->autoSizeToFitWidgets();
    gui2->setDrawBack(false);


}
//--------------------------------------------------------------
void testApp::setGUI5(){

    gui5 = new ofxUISuperCanvas("Channel 2", guiWidth*4, 0, guiWidth, guiHeight);
    gui5->setGlobalSliderHeight(10.0);
    gui5-> setWidgetFontSize(OFX_UI_FONT_SMALL);
    gui5->setGlobalGraphHeight(50.0);
    gui5->setGlobalSpacerHeight(5.0);

    gui5->addSpacer();///---------------------------------
    gui5->addLabel("INTENSITY");
	ch2.rmsSlider    = gui5->addSlider("RMS", log10(0.001), log10(1.0), log10(0.001));
	ch2.energySlider = gui5->addSlider("ENERGY", 0.0, maxEnergy, 0.0);
	ch2.powerSlider  = gui5->addSlider("POWER", log10(0.000001), log10(1.0), log10(0.001));

    vector<float> energyArray;
    for(int i = 0; i < BS; i++) energyArray.push_back(log10(0.000001));
    ch2.mgPower = gui5->addMovingGraph("EnergyGraph", energyArray, BS, log10(0.000001), log10(1.0));

    gui5->addSpacer();///------------------------------------
    gui5->addLabel("PITCH");
    ch2.mgPitch = gui5->addMovingGraph("PitchGraph", energyArray, BS, log2(minPitch), log2(maxPitch));
    ch2.pitchSlider = gui5->addSlider("PITCH Frequency", minPitch, maxPitch, 0.0);
    ch2.mgConf = gui5->addMovingGraph("ConfGraph", energyArray, BS, 0.0, 1.0);
    ch2.pitchConfSlider = gui5->addSlider("PITCH Confidence", 0.0, 1.0, 0.0);
    ch2.pitchSalienceSlider = gui5->addSlider("PITCH Salience", 0.0, 1.0, 0.0);
    ch2.tuningDialer = gui5->addNumberDialer("Tuning Freq.", 400.0, 500.0, 440.0, 1);
    ch2.centsDialer =  gui5->addNumberDialer("Cents Dev.", -35.0, 65.0, 0.0, 1);
    gui5->addSpacer();///------------------------------------
    gui5->addLabel("ONSETS");
    ch2.onsetsToggle = gui5->addToggle( "Onsets", false, 60.0, 30.0);
    ch2.onsetHfcSlider = gui5->addSlider("Onset HFC", log10(1.0), log10(maxOnHfc), 0.0);
    ch2.onsetComplexSlider = gui5->addSlider("Onset COMPLEX", 0.0, maxOnCmpx, 0.0);
    ch2.onsetFluxSlider = gui5->addSlider("Onset FLUX", 0.0, maxOnFlux, 0.0);

    gui5->autoSizeToFitWidgets();
    gui5->setDrawBack(false);

}
//--------------------------------------------------------------
void testApp::setGUI3(){

    gui3 = new ofxUISuperCanvas("Channel 1", guiWidth*2.5, 0, guiWidth, guiHeight);

    gui3->setGlobalSliderHeight(10.0);
    gui3-> setWidgetFontSize(OFX_UI_FONT_SMALL);
    gui3->setGlobalGraphHeight(50.0);
    gui3->setGlobalSpacerHeight(5.0);

    gui3->addSpacer();///----------------------------
    gui3->addLabel("SPECTRAL");
    gui3->addLabel("FFT");
    gui3->addSpectrum("Spectrum", audioAnalyzer1.getSpectrum(), BS/2, log10(0.001), log10(1.0));
    gui3->addLabel("Mel Bands");
    gui3->addSpectrum("Mel-Bands", audioAnalyzer1.getMelBands(), 24, log10(0.001), log10(1.0));
    gui3->addLabel("MFCC");
    gui3->addSpectrum("Mfcc", audioAnalyzer1.getDct(), 10, 0.0, maxDct);
    gui3->addLabel("HPCP");
    gui3->addSpectrum("Hpcp-bands", audioAnalyzer1.getHpcp(), 12, 0.0, 1.0);
    gui3->addSpacer();///----------------------------
    gui3->setGlobalGraphHeight(35.0);

    vector<float> energyArray;
    for(int i = 0; i < BS; i++) energyArray.push_back(0.0);

    ch1.mgHfc = gui3->addMovingGraph("HfcGraph", energyArray, BS, 0.0, maxHfc);
    ch1.hfcSlider = gui3->addSlider("HFC", 0.0, maxHfc, 0.0);
    ch1.mgCentroid = gui3->addMovingGraph("CentroidGraph", energyArray, BS, 0.0, maxCentroid);
    ch1.centroidSlider = gui3->addSlider("Centroid", 0.0 ,maxCentroid, 0.0);
    ch1.mgSpecComp = gui3->addMovingGraph("SpecCompGraph", energyArray, BS, 0.0, maxSpectralComp);
    ch1.specCompSlider = gui3->addSlider("Spectral Complexity", 0.0, maxSpectralComp, 0.0);
    ch1.mgInharm = gui3->addMovingGraph("InharmGraph", energyArray, BS, 0.0, 1.0);
    ch1.inharmSlider = gui3->addSlider("Inharmonicity", 0.0, 1.0, 0.0);

    gui3->autoSizeToFitWidgets();
    gui3->setDrawBack(false);


}
//--------------------------------------------------------------
void testApp::setGUI6(){

    gui6 = new ofxUISuperCanvas("Channel 2", guiWidth*5, 0, guiWidth, guiHeight);

    gui6->setGlobalSliderHeight(10.0);
    gui6-> setWidgetFontSize(OFX_UI_FONT_SMALL);
    gui6->setGlobalGraphHeight(50.0);
    gui6->setGlobalSpacerHeight(5.0);

    gui6->addSpacer();///----------------------------
    gui6->addLabel("SPECTRAL");
    gui6->addLabel("FFT");
    gui6->addSpectrum("Spectrum", audioAnalyzer2.getSpectrum(), BS/2, log10(0.001), log10(1.0));
    gui6->addLabel("Mel Bands");
    gui6->addSpectrum("Mel-Bands", audioAnalyzer2.getMelBands(), 24, log10(0.001), log10(1.0));
    gui6->addLabel("MFCC");
    gui6->addSpectrum("Mfcc", audioAnalyzer2.getDct(), 10, 0.0, maxDct);
    gui6->addLabel("HPCP");
    gui6->addSpectrum("Hpcp-bands", audioAnalyzer2.getHpcp(), 12, 0.0, 1.0);
    gui6->addSpacer();///----------------------------
    gui6->setGlobalGraphHeight(35.0);

    vector<float> energyArray;
    for(int i = 0; i < BS; i++) energyArray.push_back(0.0);

    ch2.mgHfc = gui6->addMovingGraph("HfcGraph", energyArray, BS, 0.0, maxHfc);
    ch2.hfcSlider = gui6->addSlider("HFC", 0.0, maxHfc, 0.0);
    ch2.mgCentroid = gui6->addMovingGraph("CentroidGraph", energyArray, BS, 0.0, maxCentroid);
    ch2.centroidSlider = gui6->addSlider("Centroid", 0.0, maxCentroid, 0.0);
    ch2.mgSpecComp = gui6->addMovingGraph("SpecCompGraph", energyArray, BS, 0.0, maxSpectralComp);
    ch2.specCompSlider = gui6->addSlider("Spectral Complexity", 0.0, maxSpectralComp, 0.0);
    ch2.mgInharm = gui6->addMovingGraph("InharmGraph", energyArray, BS, 0.0, 1.0);
    ch2.inharmSlider = gui6->addSlider("Inharmonicity", 0.0, 1.0, 0.0);

    gui6->autoSizeToFitWidgets();
    gui6->setDrawBack(false);


}
//--------------------------------------------------------------
void testApp::guiEvent_0(ofxUIEventArgs &e){

    string name = e.widget->getName();
	int kind = e.widget->getKind();

    static float file1LastValue = 1.0;
    static float file2LastValue = 1.0;
    static float mic1LastValue = 1.0;
    static float mic2LastValue = 1.0;

    if (name == "MICS"){
	    cout<<"MICS MODE"<<endl;
	    iMode = MICS;
	    file1LastValue=fileVolume1->getValue();
	    file2LastValue=fileVolume2->getValue();
	    fileVolume1->setValue(0.0);
	    fileVolume2->setValue(0.0);
        mic1Volume->setValue(mic1LastValue);
	    mic2Volume->setValue(mic2LastValue);
	    fileVolume1->setVisible(false);
	    fileVolume2->setVisible(false);
	    filePlay->setVisible(false);
	    fileDisplay->setVisible(false);
	    fileStop->setVisible(false);
	    textInput->setVisible(false);
	    if(!mic1Mute->getValue())mic1Volume->setVisible(true);
	    if(!mic2Mute->getValue())mic2Volume->setVisible(true);
	    mic1Mute->setVisible(true);
	    mic2Mute->setVisible(true);
	}else if (name == "AUDIO FILE"){
	    cout<<"Audio File Mode"<<endl;
	    iMode = FILES;
	    if (mic1Volume->getValue()) mic1LastValue=mic1Volume->getValue();
	    if (mic2Volume->getValue()) mic2LastValue=mic2Volume->getValue();
	    fileVolume1->setValue(file1LastValue);
	    fileVolume2->setValue(file2LastValue);
	    mic1Volume->setValue(0.0);
	    mic2Volume->setValue(0.0);
        fileVolume1->setVisible(true);
        fileVolume2->setVisible(true);
	    filePlay->setVisible(true);
	    fileStop->setVisible(true);
	    fileDisplay->setVisible(true);
	    textInput->setVisible(true);
	    mic1Volume->setVisible(false);
	    mic2Volume->setVisible(false);
	    mic1Mute->setVisible(false);
	    mic2Mute->setVisible(false);

	}else if(name == "OPEN FILE"){
        ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
        if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER) {
            cout << "ON ENTER: ";
            filename = textinput->getTextString();
            string textString = "File loaded: "+ filename;
            fileDisplay->setTextString(textString);
            //ofUnregisterKeyEvents((testApp*)this);
        }else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
            cout << "ON FOCUS: ";
        }else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_UNFOCUS) {
            cout << "ON BLUR: ";
//          ofRegisterKeyEvents(this);
        }
    }else if (name == "PLAY"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value) core.openPlayFile(filename);
    }else if (name == "STOP"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (!value) core.pd.sendBang("stopOF");
    }else if (name == "ch1 Vol"){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        file1LastValue = slider->getValue();
        float value = slider->getScaledValue();
        core.pd.sendFloat("volOF_1", value);
    }else if (name == "ch2 Vol"){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        file2LastValue = slider->getValue();
        float value = slider->getScaledValue();
        core.pd.sendFloat("volOF_2", value);
    }else if (name == "MIC-1 Vol"){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        m1vol = mic1LastValue = slider->getValue();
    }else if (name == "MIC-2 Vol"){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        m2vol = mic2LastValue = slider->getValue();
    }else if (name == "Mute-1"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bool value = toggle->getValue();
        if (value){
            mic1LastValue=mic1Volume->getValue();
            mic1Volume->setValue(0.0);
            mic1Volume->setVisible(false);
            m1vol = 0.0;
        }else{
            mic1Volume->setValue(mic1LastValue);
            mic1Volume->setVisible(true);
            m1vol = mic1LastValue;
        }
    }else if (name == "Mute-2"){
          ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bool value = toggle->getValue();
        if (value){
            mic2LastValue=mic2Volume->getValue();
            mic2Volume->setValue(0.0);
            mic2Volume->setVisible(false);
            m2vol = 0.0;
        }else{
            mic2Volume->setValue(mic2LastValue);
            mic2Volume->setVisible(true);
            m2vol = mic2LastValue;
        }
    }else if (name == "AUDIO-ON/OFF"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
		bool value = toggle->getValue();
		if (value) soundStream.start();
		else soundStream.stop();
    }else if (name == "SEND OSC"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
		bSendOSC = toggle->getValue();
    }else if (name == "SAVE"){
        ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (!value){
            vector<string> names =  ddl->getSelectedNames();
            if(names.size() && !ddl->isOpen()){
                string file1 = "./settings/" + names[0]+ "1.xml";
                string file4 = "./settings/" + names[0]+ "4.xml";
                gui1->saveSettings(file1);
                gui4->saveSettings(file4);
                cout<<"Settings Saved!"<<endl;
            }
        }
         
    }else if (name == "LOAD"){
        ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (!value){
		    
         vector<string> names =  ddl->getSelectedNames();
            
            if(names.size() && !ddl->isOpen()){
                string file1 = "./settings/" + names[0]+ "1.xml";
                string file4 = "./settings/" + names[0]+ "4.xml";
                gui1->loadSettings(file1);
                gui4->loadSettings(file4);
                
                settings_name = file1;
                
                audioAnalyzer1.resetOnsets();
                audioAnalyzer2.resetOnsets();
                
                cout<<"Onsets Reseted"<<endl;
                cout<<"Settings Loaded: "<<settings_name<<endl;
                
                
            }
        }
    }else if (name == "NARROW VIEW"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
		bNarrowView = toggle->getValue();
        gui1->setVisible(!bNarrowView);
        gui2->setVisible(!bNarrowView);
        gui3->setVisible(!bNarrowView);
        gui4->setVisible(!bNarrowView);
        gui5->setVisible(!bNarrowView);
        gui6->setVisible(!bNarrowView);
        if(bNarrowView) ofSetWindowShape(guiWidth, ofGetHeight());
        else ofSetWindowShape(guiWidth*6, ofGetHeight());
    }
    
}
//--------------------------------------------------------------
void testApp::guiEvent_1(ofxUIEventArgs &e){
    string name = e.widget->getName();
	int kind = e.widget->getKind();

	//toggle kind=2
	//minimalSlider = 25;

	if (kind == 2){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bool value = toggle->getValue();

        if (name == "RMS"){
            audioAnalyzer1.doRms = value;
        }else if (name == "Energy"){
            audioAnalyzer1.doEnergy = value;
        }else if (name == "Power"){
            audioAnalyzer1.doPower = value;
        }else if (name == "Pitch"){
            audioAnalyzer1.doPitch = value;
        }else if (name == "Tuning"){
            audioAnalyzer1.doTuning = value;
        }else if (name == "Onsets"){
            audioAnalyzer1.doOnsets = value;
        } else if (name == "MelBands"){
            audioAnalyzer1.doMelbands = value;
        } else if (name == "MFCC"){
            audioAnalyzer1.doMfcc = value;
        } else if (name == "HPCP"){
            audioAnalyzer1.doHpcp = value;
        } else if (name == "HFC"){
            audioAnalyzer1.doHfc = value;
        } else if (name == "Centroid"){
            audioAnalyzer1.doCentroid = value;
        } else if (name == "SpcCmplx"){
            audioAnalyzer1.doSpcCmplx = value;
        } else if (name == "Inharmon"){
            audioAnalyzer1.doInharmon = value;
        } else if(name == "Hfc"){
            audioAnalyzer1.addHfc = value;
        }else if(name == "Complex"){
            audioAnalyzer1.addComplex = value;
        }else if(name == "Flux"){
            audioAnalyzer1.addFlux = value;
        }

    }else if(kind==25){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        float value = slider->getValue();
        if(name=="Alpha"){
            audioAnalyzer1.setOnsetAlpha(value);
        }else if(name=="Treshold"){
            audioAnalyzer1.setOnsetTreshold(value);
        }
    }
    
    if(name=="resetOnsets"){
        ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value) audioAnalyzer1.resetOnsets();
    }




}
//--------------------------------------------------------------
void testApp::guiEvent_4(ofxUIEventArgs &e){
    string name = e.widget->getName();
	int kind = e.widget->getKind();

	/*
    toggle kind=2
    minimalSlider = 25;
    */

	if (kind == 2){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bool value = toggle->getValue();

        if (name == "RMS"){
            audioAnalyzer2.doRms = value;
        }else if (name == "Energy"){
            audioAnalyzer2.doEnergy = value;
        }else if (name == "Power"){
            audioAnalyzer2.doPower = value;
        }else if (name == "Pitch"){
            audioAnalyzer2.doPitch = value;
        }else if (name == "Tuning"){
            audioAnalyzer2.doTuning = value;
        }else if (name == "Onsets"){
            audioAnalyzer2.doOnsets = value;
        } else if (name == "MelBands"){
            audioAnalyzer2.doMelbands = value;
        } else if (name == "MFCC"){
            audioAnalyzer2.doMfcc = value;
        } else if (name == "HPCP"){
            audioAnalyzer2.doHpcp = value;
        } else if (name == "HFC"){
            audioAnalyzer2.doHfc = value;
        } else if (name == "Centroid"){
            audioAnalyzer2.doCentroid = value;
        } else if (name == "SpcCmplx"){
            audioAnalyzer2.doSpcCmplx = value;
        } else if (name == "Inharmon"){
            audioAnalyzer2.doInharmon = value;
        } else if(name == "Hfc"){
            audioAnalyzer2.addHfc = value;
        }else if(name == "Complex"){
            audioAnalyzer2.addComplex = value;
        }else if(name == "Flux"){
            audioAnalyzer2.addFlux = value;
        }

    }else if(kind==25){
        ofxUIMinimalSlider *slider = (ofxUIMinimalSlider *) e.widget;
        float value = slider->getValue();
        if(name=="Alpha"){
            audioAnalyzer2.setOnsetAlpha(value);
        }else if(name=="Treshold"){
            audioAnalyzer2.setOnsetTreshold(value);
        }
    }
    
    if(name=="resetOnsets"){
        ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value) audioAnalyzer2.resetOnsets();
    }


}
//--------------------------------------------------------------
void testApp::keyPressed(int key){
    if(gui0->hasKeyboardFocus())
    {
        return;
    }

    switch(key){
        case 'h':
            bShowHelp = !bShowHelp;
            break;
        case 'n':
            bNarrowView = !bNarrowView;
            narrowView->setValue(bNarrowView);
            gui1->setVisible(!bNarrowView);
            gui2->setVisible(!bNarrowView);
            gui3->setVisible(!bNarrowView);
            gui4->setVisible(!bNarrowView);
            gui5->setVisible(!bNarrowView);
            gui6->setVisible(!bNarrowView);
            if(bNarrowView) ofSetWindowShape(guiWidth, ofGetHeight());
            else ofSetWindowShape(guiWidth*6, 700);
            break;
        default:
            break;
      
    }

}
//--------------------------------------------------------------
void testApp::audioIn(float * input, int bufferSize, int nChannels){
    
    if (iMode == MICS){
            for (int i = 0; i < bufferSize; i++){
                buffer_1[i]	= input[i*nChannels]*m1vol;
                buffer_2[i]	= input[i*nChannels+1]*m2vol;
            }
            audioAnalyzer1.analyze(buffer_1, bufferSize);
            audioAnalyzer2.analyze(buffer_2, bufferSize);
        }
}
//------------------------------------------------------------------
void testApp::audioOut(float * output, int bufferSize, int nChannels){
    
        if (iMode == FILES){
            core.audioRequested(output, bufferSize, nChannels);
            //ANALYZE ESSENTIA ***********************************
            for (int i = 0; i < bufferSize; i++){
                buffer_1[i]	= output[i*nChannels];
                buffer_2[i]	= output[i*nChannels+1];
            }
            audioAnalyzer1.analyze(buffer_1, bufferSize);
            audioAnalyzer2.analyze(buffer_2, bufferSize);
         
            
        }

}
//--------------------------------------------------------------
void testApp::deletePointers(){
    delete gui0;
    delete gui1;
    delete gui2;
    delete gui3;
    delete gui4;
    delete gui5;
    delete gui6;
    //delete gui7;
}
//--------------------------------------------------------------
void testApp::ch1update(){
    
        if(gui2!=NULL){
            ///Intensity-------------------
            ch1.intenSmthAmnt = ch1.intenSmooth->getValue();
            ch1.smthRms    = smooth(ch1.smthRms, audioAnalyzer1.getRms(), ch1.intenSmthAmnt);
            ch1.smthEnergy = smooth(ch1.smthEnergy, audioAnalyzer1.getEnergy(), ch1.intenSmthAmnt);
            ch1.smthPower  = smooth(ch1.smthPower, audioAnalyzer1.getPower(), ch1.intenSmthAmnt);
            ch1.rmsSlider->setValue(log10(ch1.smthRms));
            ch1.energySlider->setValue(ch1.smthEnergy);
            ch1.powerSlider->setValue(log10(ch1.smthPower));
            ch1.mgPower->addPoint(log10(ch1.smthPower));
            ///Pitch--------------------------
            ch1.pitchSmthAmnt = ch1.pitchSmooth->getValue();
            ch1.smthPitch      = smooth(ch1.smthPitch, audioAnalyzer1.getPitchFreq(), ch1.pitchSmthAmnt);
            ch1.smthConfidence = smooth(ch1.smthConfidence, audioAnalyzer1.getPitchConf(), ch1.pitchSmthAmnt);
            ch1.smthSalience   = smooth(ch1.smthSalience, audioAnalyzer1.getSalience(), ch1.pitchSmthAmnt);
            ch1.pitchSlider->setValue(ch1.smthPitch);
            ch1.mgPitch->addPoint(log2(ch1.smthPitch));
            ch1.pitchConfSlider->setValue(ch1.smthConfidence);
            ch1.mgConf->addPoint(ch1.smthConfidence);
            ch1.pitchSalienceSlider->setValue(ch1.smthSalience);
            //-
            ch1.tuningDialer->setValue(audioAnalyzer1.getTuningFreq());
            ch1.centsDialer->setValue(audioAnalyzer1.getTuningCents());
            ///ONSETS-----------------
            ch1.onsetHfcSlider->setValue(log10(audioAnalyzer1.getOnsetHfc()));
            ch1.onsetComplexSlider->setValue(audioAnalyzer1.getOnsetComplex());
            ch1.onsetFluxSlider->setValue(audioAnalyzer1.getOnsetFlux());
            ch1.onsetsToggle->setValue(audioAnalyzer1.getIsOnset());
        }
        if(gui3!=NULL){
            ///SPECTRAL----------------
            ch1.specSmthAmnt = ch1.specSmooth->getValue();
            ch1.smthHfc      = smooth(ch1.smthHfc, audioAnalyzer1.getHfc(), ch1.specSmthAmnt);
            ch1.smthCentroid = smooth(ch1.smthCentroid, audioAnalyzer1.getCentroid(), ch1.specSmthAmnt);
            ch1.smthCmplx    = smooth(ch1.smthCmplx, audioAnalyzer1.getSpectralComplex(), ch1.specSmthAmnt);
            ch1.smthInharmon = smooth(ch1.smthInharmon, audioAnalyzer1.getInharmonicity(), ch1.specSmthAmnt);

            ch1.hfcSlider->setValue(ch1.smthHfc);
            ch1.mgHfc->addPoint(ch1.smthHfc);
            ch1.centroidSlider->setValue(ch1.smthCentroid);
            ch1.mgCentroid->addPoint(ch1.smthCentroid);
            ch1.specCompSlider->setValue(ch1.smthCmplx);
            ch1.mgSpecComp->addPoint(ch1.smthCmplx);
            ch1.inharmSlider->setValue(ch1.smthInharmon);
            ch1.mgInharm->addPoint(ch1.smthInharmon);
        }


}
//--------------------------------------------------------------
void testApp::ch2update(){

     if(gui5!=NULL){
            ///Intensity-------------------
            ch2.intenSmthAmnt = ch2.intenSmooth->getValue();
            ch2.smthRms    = smooth(ch2.smthRms, audioAnalyzer2.getRms(), ch2.intenSmthAmnt);
            ch2.smthEnergy = smooth(ch2.smthEnergy, audioAnalyzer2.getEnergy(), ch2.intenSmthAmnt);
            ch2.smthPower  = smooth(ch2.smthPower, audioAnalyzer2.getPower(), ch2.intenSmthAmnt);
            ch2.rmsSlider->setValue(log10(ch2.smthRms));
            ch2.energySlider->setValue(ch2.smthEnergy);
            ch2.powerSlider->setValue(log10(ch2.smthPower));
            ch2.mgPower->addPoint(log10(ch2.smthPower));
            ///Pitch--------------------------
            ch2.pitchSmthAmnt = ch2.pitchSmooth->getValue();
            ch2.smthPitch      = smooth(ch2.smthPitch, audioAnalyzer2.getPitchFreq(), ch2.pitchSmthAmnt);
            ch2.smthConfidence = smooth(ch2.smthConfidence, audioAnalyzer2.getPitchConf(), ch2.pitchSmthAmnt);
            ch2.smthSalience   = smooth(ch2.smthSalience, audioAnalyzer2.getSalience(), ch2.pitchSmthAmnt);
            ch2.pitchSlider->setValue(ch2.smthPitch);
            ch2.mgPitch->addPoint(log2(ch2.smthPitch));
            ch2.pitchConfSlider->setValue(ch2.smthConfidence);
            ch2.mgConf->addPoint(ch2.smthConfidence);
            ch2.pitchSalienceSlider->setValue(ch2.smthSalience);
            //-
            ch2.tuningDialer->setValue(audioAnalyzer2.getTuningFreq());
            ch2.centsDialer->setValue(audioAnalyzer2.getTuningCents());
            ///ONSETS-----------------
            ch2.onsetHfcSlider->setValue(log10(audioAnalyzer2.getOnsetHfc()));
            ch2.onsetComplexSlider->setValue(audioAnalyzer2.getOnsetComplex());
            ch2.onsetFluxSlider->setValue(audioAnalyzer2.getOnsetFlux());
            ch2.onsetsToggle->setValue(audioAnalyzer2.getIsOnset());
        }

        if(gui6!=NULL){
            ///SPECTRAL----------------
            ch2.specSmthAmnt = ch2.specSmooth->getValue();
            ch2.smthHfc      = smooth(ch2.smthHfc, audioAnalyzer2.getHfc(), ch2.specSmthAmnt);
            ch2.smthCentroid = smooth(ch2.smthCentroid, audioAnalyzer2.getCentroid(), ch2.specSmthAmnt);
            ch2.smthCmplx    = smooth(ch2.smthCmplx, audioAnalyzer2.getSpectralComplex(), ch2.specSmthAmnt);
            ch2.smthInharmon = smooth(ch2.smthInharmon, audioAnalyzer2.getInharmonicity(), ch2.specSmthAmnt);

            ch2.hfcSlider->setValue(ch2.smthHfc);
            ch2.mgHfc->addPoint(ch2.smthHfc);
            ch2.centroidSlider->setValue(ch2.smthCentroid);
            ch2.mgCentroid->addPoint(ch2.smthCentroid);
            ch2.specCompSlider->setValue(ch2.smthCmplx);
            ch2.mgSpecComp->addPoint(ch2.smthCmplx);
            ch2.inharmSlider->setValue(ch2.smthInharmon);
            ch2.mgInharm->addPoint(ch2.smthInharmon);
        }

}
//--------------------------------------------------------------
float testApp::smooth(float last, float value,  float amnt){
    float result;
    result = last * amnt;
    result += (1-amnt) * value;
    return result;
}
//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}


