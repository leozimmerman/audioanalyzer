#pragma once

#include "ofMain.h"
#include "AppCore.h"
#include "ofxUI.h"
#include "ofxAudioAnalyzer.h"

#define SR 44100 /// SAMPLE RATE
#define BS 512   /// BUFFER SIZE
#define I_CH 2   /// INPUT CHANNELS
#define O_CH 2   /// OUTPUT CHANNELS

#define CLARO 86,55,48
#define OSCURO 86,68,48
#define GRIS 67,67,67
#define NEGRO 0,0,0

#define HOST "localhost"
#define SEND_PORT 12345
#define RECEIVE_PORT 54321

enum {MICS, FILES};

struct ChannelGui{
        ofxUISlider* rmsSlider;
        ofxUISlider* energySlider;
        ofxUISlider* powerSlider;
        ofxUIMinimalSlider *intenSmooth;
        //-
        ofxUISlider* pitchSlider;
        ofxUISlider* pitchConfSlider;
        ofxUISlider* pitchSalienceSlider;
        ofxUIMinimalSlider *pitchSmooth;
        ofxUISlider* hfcSlider;
        ofxUISlider* centroidSlider;
        ofxUISlider* specCompSlider;
        //----------
        ofxUIToggle *onsetsToggle;
        ofxUISlider* onsetHfcSlider;
        ofxUISlider* onsetComplexSlider;
        ofxUISlider* onsetFluxSlider;
        ofxUISlider* inharmSlider;
        ofxUIMinimalSlider* specSmooth;
        //--------------
        ofxUINumberDialer* tuningDialer;
        ofxUINumberDialer* centsDialer;

        ofxUIMovingGraph *mgPower;
        ofxUIMovingGraph *mgPitch;
        ofxUIMovingGraph *mgConf;
        ofxUIMovingGraph *mgHfc;
        ofxUIMovingGraph *mgCentroid;
        ofxUIMovingGraph *mgSpecComp;
        ofxUIMovingGraph *mgInharm;

        float intenSmthAmnt, pitchSmthAmnt, specSmthAmnt;
        float smthPower, smthEnergy, smthRms;
        float smthPitch, smthConfidence, smthSalience;
        float smthHfc, smthCentroid, smthCmplx, smthInharmon;

};

class testApp : public ofBaseApp{

	public:

		void setup();
		void update();
		void draw();
		void exit();

		void keyPressed  (int key);
		void windowResized(int w, int h);

        void deletePointers();
        void essentiaSetup();
		void processEssentia(float * iBuffer, int bufferSize);
		void ch1update();
		void ch2update();

		void audioIn(float * input, int bufferSize, int nChannels);
        void audioOut(float * input, int bufferSize, int nChannels);
    
        float smooth(float last, float value,  float amnt);

        void setGUI0();
        void setGUI1();
        void setGUI2();
        void setGUI3();
        void setGUI4();
        void setGUI5();
        void setGUI6();
        void setGUI7();

        ofSoundStream soundStream;
        AppCore core;
        ofxAudioAnalyzer audioAnalyzer1;
        ofxAudioAnalyzer audioAnalyzer2;
        ChannelGui ch1;
        ChannelGui ch2;

        string monitorInfo;

        ofxUISuperCanvas *gui0;
        ofxUISuperCanvas *gui1;
        ofxUISuperCanvas *gui2;
        ofxUISuperCanvas *gui3;
        ofxUISuperCanvas *gui4;
        ofxUISuperCanvas *gui5;
        ofxUISuperCanvas *gui6;
        ofxUISuperCanvas *gui7;

        void guiEvent_0(ofxUIEventArgs &e);
        void guiEvent_1(ofxUIEventArgs &e);
        void guiEvent_4(ofxUIEventArgs &e);

        ofxUITextInput *textInput;
        ofxUITextArea *fileDisplay;
        ofxUIMinimalSlider *fileVolume1;
        ofxUIMinimalSlider *fileVolume2;
        ofxUIMinimalSlider *mic1Volume;
        ofxUIMinimalSlider *mic2Volume;
        ofxUIToggle *mic1Mute;
        ofxUIToggle *mic2Mute;
        ofxUIButton *filePlay;
        ofxUIButton *fileStop;
        ofxUIToggle *narrowView;
        ofxUIDropDownList *ddl;

        string filename;
        string settings_name;
        float guiWidth;
        float guiHeight;

        float *buffer_1;
        float *buffer_2;
    

        int iMode;
        int chOffset;

        float m1vol, m2vol;

        float  maxEnergy, minPitch, maxPitch, maxHfc, maxCentroid,
        maxSpectralComp, maxOnHfc, maxOnCmpx, maxOnFlux, maxDct;

        bool bSendOSC, bShowHelp, bNarrowView;


};
